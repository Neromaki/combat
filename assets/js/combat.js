/*
*	THINGS TO DO:
*	- DONE!		Hash attackPower out into two seperate entities: hitChance and Damage
*	- DONE!		When Player and Enemy both defend for a turn, have the timeout only last one or two seconds (instead of the full 4)
*	- Have a few switch statements for each line of output to randomise the text being used

*	- Assign each move a "priority" or "speed" variable to determine the flow of combat
*
*	+ IN PROGRESS	Add a MAGIC attack
*		- Thus add MANA, which slowly recharges over rounds
*
*	+ IN PROGRESS	Add experience for each enemy defeated?
		- Make enemy XP rewards a table column in combat_enemies
*/

var enemies = [
	{
		name: 		"Slime",
		level: 		1,
		health:		10,
		xp_reward:	8
	},
	{
		name: 		"Goblin",
		level: 		2,
		health:		25,
		xp_reward:  22
	},
	{
		name: 		"Skeleton",
		level: 		3,
		health:		50,
		xp_reward:  46
	},
	{
		name: 		"Troll",
		level: 		4,
		health:		75,
		xp_reward:  70
	},
	{
		name: 		"Ogre",
		level: 		5,
		health:		100,
		xp_reward:  94
	}
];


/*
*	Player object, currently everything is set statically
*/
var Player = function(xp) {
	this.action = "";
	this.move = 0;
	this.attackPower = 0;
	this.tempLevel = 0;


	this.checkLevel = function(xp) {
		switch(true)
		{
			case(xp >= 0 && xp <= 29):
				this.level = 1;
				this.max_health 	= 50;
				if(this.tempLevel != this.level) {
					this.health	= 50;
				}
				this.max_mana 		= 10;
				if(this.tempLevel != this.level) {
					this.mana = 10;
				}
				this.xp 			= xp;
				this.xp_ascension 	= 30;
			break;

			case(xp >= 30 && xp <= 69):
				this.level = 2;
				this.max_health 	= 55;
				if(this.tempLevel != this.level) {
					this.health	= 55;
				}
				this.max_mana 		= 15;
				if(this.tempLevel != this.level) {
					this.mana = 15;
				}
				this.xp 			= xp;
				this.xp_ascension 	= 70;
			break;

			case(xp >= 70 && xp <= 119):
				this.level = 3;
				this.max_health 	= 60;
				if(this.tempLevel != this.level) {
					this.health = 60;
				}
				this.max_mana 		= 20;
				if(this.tempLevel != this.level) {
					this.mana = 20;
				}
				this.xp 			= xp;
				this.xp_ascension 	= 120;
			break;

			default:
				this.level = 4;
				this.max_health 	= 70;
				if(this.tempLevel != this.level) {
					this.health = 70;
				}
				this.max_mana 		= 25;
				if(this.tempLevel != this.level) {
					this.mana = 25;
				}
				this.xp 			= xp;
				this.xp_ascension 	= 99999;
			break;
		}

		this.tempLevel = this.level;
	}


	this.checkLevel(xp);

	/*
	*	PLAYER ATTACK
	*/
	this.attack = function() {
		if(this.move === 1) {
			var delay = 0;

			print_output('output', "You maneuver to attack<br>", true);
		}
		else {
			var delay = 2000;

			window.setTimeout(function () {
				print_output('output', "You maneuver to attack<br>", false)
			}, delay);
		}

		//var hc1 = Math.random();
		//var hc2 = hc1 + 0.2;
		//var hc3 = Math.round(hc2);
		//var hitChance = hc3;

		// Hit chance (the +0.3 gives the player an 80% hit rate)
		var hitChance = Math.round(Math.random() + 0.3);
		//console.log("Hit chance: HC1 = " + hc1 + ", HC2 = " + hc2 + ", HC3 = " + hc3);

		// Attack power
		var attack = Math.ceil(Math.random() * (this.level + 3));
		if (hitChance)
		{
			this.attackPower = attack;

			window.setTimeout(function () {
				print_output('output', "You strike the " + enemy.name + " for " + attack + " damage!<br>", false)
			}, delay+1000);

			enemy.health -= attack;
			if (enemy.health < 0)
			{
				enemy.health = 0;
			}

		}
		else
		{
			window.setTimeout(function () {
				print_output('output', "You swing at the " + enemy.name + " and miss!<br>", false)
			}, delay+1000);
		}
	}


	/*
	*	PLAYER MAGIC ATTACK
	*/
	this.magic_attack = function() {
		if(this.move === 1) {
			var delay = 0;

			print_output('output', "You channel your magical energies for an attack<br>", true);
		}
		else {
			var delay = 2000;

			window.setTimeout(function () {
				print_output('output', "You channel your magical energies for an attack<br>", false)
			}, delay);
		}

		//var hc1 = Math.random();
		//var hc2 = hc1 + 0.2;
		//var hc3 = Math.round(hc2);
		//var hitChance = hc3;

		// Hit chance (the +0.3 gives the player an 80% hit rate)
		var hitChance = Math.round(Math.random() + 0.3);
		//console.log("Hit chance: HC1 = " + hc1 + ", HC2 = " + hc2 + ", HC3 = " + hc3);

		// Attack power
		var attack = Math.floor(Math.random() * 11);
		if (hitChance)
		{
			this.attackPower = attack;

			window.setTimeout(function () {
				print_output('output', "You lash out with a torrent of magical energy and strike the " + enemy.name + " for " + attack + " damage!<br>", false)
			}, delay+1000);

			enemy.health -= attack;

			if (enemy.health < 0)
			{
				enemy.health = 0;
			}

		}
		else
		{
			window.setTimeout(function () {
				print_output('output', "You throw a bolt of magical energy at the " + enemy.name + " and miss!<br>", false)
			}, delay+1000);
		}

		this.mana -= 8;
	}


	/*
	*	PLAYER DEFEND
	*/
	this.defend = function() {
		if(player.move === 1) {
			var delay = 0;
			print_output('output', "You maneuver to defend<br>", true);
		}
		else {
			var delay = 2000;
			window.setTimeout(function () {
				print_output('output', "You maneuver to defend<br>", false)
			}, delay);
		}

		if(enemy.attackPower > 0)
		{
			var playerDefend = Math.floor(Math.random() * enemy.attackPower + 0.5);
			if (playerDefend)
			{
				/*this.health -= playerDefend;*/
				if (this.health < 0)
				{
					this.health = 0;
				}

				window.setTimeout(function () {
					print_output('output', "You raise your shield and manage to deflect " + playerDefend + " damage back at the " + enemy.name + ", causing you to only take " + (enemy.attackPower - playerDefend) + " damage.<br>", false)
				}, delay+1000);
				this.health += playerDefend;
				enemy.health -= playerDefend;
			}
			else
			{
				window.setTimeout(function () {
					print_output('output', "You attempt to parry the " + enemy.name + "'s attack, but you fail.<br>", false)
				}, delay+1000);
			}
		}
	}
}


var Enemy = function (name, level, health, xp_reward, sprite) {
	this.action = "";
	this.move = 0;
	this.attackPower = 0;

	this.name = name;
	this.level = level;
	this.health = health;
	this.xp_reward = xp_reward;
	document.getElementById('enemy_sprite').innerHTML = "<img class='enemy' id='" + name.toLowerCase() + "' src='" + sprite + "' />";

	this.max_health = this.health;

	/*
	*	ENEMY ATTACK
	*/
	this.attack = function() {
		if(this.health > 0)
		{
			if(enemy.move === 1) {
				var delay = 0;
				print_output('output', "The enemy maneuvers to attack<br>", true);

			}
			else {
				var delay = 2000;
				window.setTimeout(function () {
					print_output('output', "The enemy maneuvers to attack<br>", false)
				}, delay);
			}

			var hitChance = Math.round(Math.random() + 0.1);
			this.attackPower = Math.round((Math.random() * 3) * this.level);

			if (hitChance)
			{
				window.setTimeout(function () {
					print_output('output', "The " + enemy.name + " strikes you for " + enemy.attackPower + " damage!<br>", false)
				}, delay+1000);
				player.health -= this.attackPower;
			}
			else
			{
				window.setTimeout(function () {
					print_output('output', "The " + enemy.name + " swings at you and misses!<br>", false)
				}, delay+1000);
			}
		}
	}


	/*
	*	ENEMY DEFEND
	*/
	this.defend = function() {
		if(this.health > 0)
		{
			if(enemy.move === 1) {
				var delay = 0;
				window.setTimeout(function () {
					print_output('output', "The enemy maneuvers to defend<br>", true)
				}, delay);
			}
			else {
				var delay = 2000;
				window.setTimeout(function () {
					print_output('output', "The enemy maneuvers to defend<br>", false)
				}, delay);
			}

			if(player.attackPower > 0)
			{
				var enemyDefend = Math.floor(Math.random() * player.attackPower + 0.5);

				if (enemyDefend)
				{
					player.attackPower -= enemyDefend;
					if (player.attackPower < 0)
					{
						player.attackPower = 0;
					}
					window.setTimeout(function () {
						print_output('output', "The enemy deflects " + enemyDefend + " damage back, causing you to only inflict " + player.attackPower + " damage.<br>", false)
					}, delay+1000);
					player.health -= enemyDefend;
					this.health += enemyDefend;
				}
				else
				{
					window.setTimeout(function () {
						print_output('output',  "The " + enemy.name + " tried to block your attack, but failed.<br>", false)
					}, delay+1000);
				}
			}
			else
			{
				window.setTimeout(function () {
					print_output('output',  "The " + enemy.name + " dodges your blow.<br>", false)
				}, delay+1000);
			}
		}
	}
}


var print_output = function(element, output, override)
{
	if(override === true) {
		document.getElementById(element).innerHTML = output;
	}
	else {
		document.getElementById(element).innerHTML += output;
	}
}


var update_gui = function(gui)
{
	if(gui === 'player') {
		player.checkLevel(player.xp);
		document.getElementById('player_hp').innerHTML = player.health;
		var playerHealthRemaining = (player.health / player.max_health) * 100;
		document.getElementById('player_hp_bar').style.width = playerHealthRemaining + "%";

		document.getElementById('player_mp').innerHTML = player.mana;
		var playerManaRemaining = (player.mana / player.max_mana) * 100;
		document.getElementById('player_mp_bar').style.width = playerManaRemaining + "%";

		//document.getElementById('player_xp').innerHTML = player.xp + " (Level " + player.level + ")";
	}

	if(gui === 'enemy') {
		document.getElementById('enemy_hp').innerHTML = enemy.health;
		var enemyHealthRemaining = (enemy.health / enemy.max_health) * 100;
		document.getElementById('enemy_hp_bar').style.width = enemyHealthRemaining + "%";
	}
}


var toggle_buttons = function(override)
{
	if(this.turn > 1)
		$("#random_enemy").hide();
	else
		$("#random_enemy").show();

	if(document.getElementById("attack").disabled === true) {
		document.getElementById("attack").disabled = false;
		if(player.mana >= 5) {
			document.getElementById("magic").disabled = false;
		}
		document.getElementById("defend").disabled = false;
	}
	else {
		document.getElementById("attack").disabled = true;
			document.getElementById("magic").disabled = true;

		document.getElementById("defend").disabled = true;
	}
}


var process_round = function()
{
	toggle_buttons();

	console.log(player.health);

	if (player.health > 0 && enemy.health > 0)
	{
		this.turn++;
		document.getElementById('turn').innerHTML = " " + this.turn;

		var enemyIntent = Math.round(Math.random() + 0.85);

		switch(enemyIntent)
		{
			case(1):
				enemy.action = 'attack';
			break;

			case(2):
				enemy.action = 'defend';;
			break;

			default:
				enemy.action = 'attack';
			break;
		}

		if((player.action === 'attack' || player.action === 'magic') && enemy.action === 'defend') {
			player.move = 1;
			enemy.move = 2;
			window.setTimeout(function () { update_gui('player') }, 3000);
			window.setTimeout(function () { update_gui('enemy') }, 3000);
			if(player.action === 'attack') {
				player.attack();
			}
			else if(player.action === 'magic') {
				player.magic_attack();
			}
			enemy.defend();
			window.setTimeout(function() { toggle_buttons() }, 4250);
		}
		else if(player.action === 'defend' && enemy.action === 'attack') {
			player.move = 2;
			enemy.move = 1;
			window.setTimeout(function () { update_gui('player') }, 3000);
			window.setTimeout(function () { update_gui('enemy') }, 3000);
			enemy.attack();
			player.defend();
			window.setTimeout(function() { toggle_buttons() }, 4250);
		}
		else if(player.action === 'defend' && enemy.action === 'defend') {
			document.getElementById('output').innerHTML = "You both raise your shields to defend, and spend a few awkwardly tense moments staring each other down<br>";
			window.setTimeout(function() { toggle_buttons() }, 1000);
		}
		else {
			player.move = 1;
			enemy.move = 2;
			window.setTimeout(function () { update_gui('player') }, 3000);
			window.setTimeout(function () { update_gui('enemy') }, 1000);
			if(player.action === 'attack') {
				player.attack();
			}
			else if(player.action === 'magic') {
				player.magic_attack();
			}
			enemy.attack();
			window.setTimeout(function() { toggle_buttons() }, 4250);
		}

		if (player.health <= 0)
		{
			window.setTimeout(function () {
				print_output('output',  "<br>You were slain by the " + enemy.name + "!<br><strong><a href=''>Laugh in the face of death, and resurrect to fight another!</a></strong>", false)
			}, 4000);

			window.setTimeout(function () {
				print_output('output',  "<br><strong><a href=''>Laugh in the face of death, and resurrect to fight another!</a></strong>", false)
			}, 4500);
			return;
		}
		else if (enemy.health <= 0)
		{
			window.setTimeout(function () {
				print_output('output',  "<br><strong><a href=''>I want to fight another!</a></strong>", false)
			}, 4500);

			var data = {xp: enemy.xp_reward};
			$.post(window.location.pathname + '/save_progress', data, function(returnedData)
			{
				player.checkLevel(returnedData);
				/*
				window.setTimeout(function () {
					document.getElementById('player_xp').innerHTML = returnedData;
				}, 3500);
				*/
				console.log(returnedData);
			});

			/*
			window.setTimeout(function () {
				print_output('output', "<br>You have gained " + enemy.xp_reward + " XP!", false)
			}, 4250);
			*/

			$("#random_enemy").show();
		}

		player.mana_regen = 2;
		if((player.mana + player.mana_regen) < player.max_mana)
		{
			window.setTimeout(function () {
				player.mana += 2;
			}, 4500);

			window.setTimeout(function () {
				print_output('output', "You regenerate some mana.<br>", false)
			}, 4500);
			window.setTimeout(function () { update_gui('player') }, 4500);
		}
	}
}
