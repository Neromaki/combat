(function($) {

	$.fn.charCount = function(options){
	  
		// default configuration properties
		var defaults = {	
			allowed: 140,		
			warning: 25,
			messages: 1,
			css: 'counter',
			counterElement: 'span',
			cssWarning: 'warning',
			cssExceeded: 'exceeded',
			counterText: ''
		}; 
			
		
		function roundMessages(num) { 
            var roundedMessages = Math.ceil(num * Math.pow(10, 0)) / Math.pow(10, 0);
            return roundedMessages;
        }
		
		var options = $.extend(defaults, options); 
		
		function calculate(obj)
		{
			var count = $(obj).val().length;
			var available = count;
			
			if(available <= options.warning && available >= 0)
			{
				/*$(obj).next().addClass(options.cssWarning);*/
			} 
			else 
			{
				/*$(obj).next().removeClass(options.cssWarning);*/
			}
			if(available > options.allowed)
			{
				options.messages = roundMessages((available / options.allowed), 0);
				options.counterText = ' (' + options.messages + ' msgs)';
			} 
			else 
			{
				/*$(obj).next().removeClass(options.cssExceeded);*/
				options.counterText = '';
			}
			$(obj).next().html(available + options.counterText);
		};
				
		this.each(function() {  			
			$(this).after('<'+ options.counterElement +' class="' + options.css + '">'+ options.counterText +'</'+ options.counterElement +'>');
			calculate(this);
			$(this).keyup(function(){calculate(this)});
			$(this).change(function(){calculate(this)});
		});
	  
	};

})(jQuery);

/*
(function($) {

	$.fn.charCount = function(options){
	  
		// default configuration properties
		var defaults = {	
			allowed: 140,		
			warning: 25,
			messages: 1,
			css: 'counter',
			counterElement: 'span',
			cssWarning: 'warning',
			cssExceeded: 'exceeded',
			counterText: ''
		}; 
			
		var options = $.extend(defaults, options); 
		
		function calculate(obj){
			var count = $(obj).val().length;
			var available = options.allowed - count;
			if(available <= options.warning && available >= 0){
				$(obj).next().addClass(options.cssWarning);
			} else {
				$(obj).next().removeClass(options.cssWarning);
			}
			if(available < 0){
				$(obj).next().addClass(options.cssExceeded);
				options.counterText = '';
			} else {
				$(obj).next().removeClass(options.cssExceeded);
				options.counterText = '';
			}
			$(obj).next().html(available + options.counterText);
		};
				
		this.each(function() {  			
			$(this).after('<'+ options.counterElement +' class="' + options.css + '">'+ options.counterText +'</'+ options.counterElement +'>');
			calculate(this);
			$(this).keyup(function(){calculate(this)});
			$(this).change(function(){calculate(this)});
		});
	  
	};

})(jQuery);
*/